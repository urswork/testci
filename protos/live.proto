syntax = "proto3";

package olimpapi;

import "main.proto";

import "google/api/annotations.proto";
import "google/protobuf/wrappers.proto";

message LiveChampRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  int64 id = 2; // Идентификатор чемпионата
  int64 toHours = 3; // период для выбора в часах
}

message LiveChampVersionedRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  int64 id = 2;  // Идентификатор чемпионата
  google.protobuf.Int64Value version = 3; // Версия данных у клиента. Поле не заполняется (null) при отсутствии данных.
}

message LiveChampsAllRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  google.protobuf.Int64Value version = 2; // Версия данных у клиента. Поле не заполняется (null) при отсутствии данных.
}

message LiveChampsRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  repeated VersionedIdClient ids = 2;  // id чемпионата и версия данных 
}

message LiveEventRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  int64 id = 2;  // Идентификатор события
  google.protobuf.Int64Value version = 3; // Версия данных у клиента. Поле не заполняется (null) при отсутствии данных.
}

message LiveEventsRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  bool main = 2; // флаг если true, то выводятся только основные исходы (короткая роспись), если false то все исходы (полная роспись).
  google.protobuf.Int64Value version = 3; // Версия данных у клиента. Поле не заполняется (null) при отсутствии данных.
  repeated VersionedIdClient ids = 4;
}

message LiveSportRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  int64 id = 2; // Идентификатор вида спорта
  google.protobuf.Int64Value version = 3; // Версия данных у клиента. Поле не заполняется (null) при отсутствии данных.
}

message LiveSportChampsRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  int64 id = 2; // Идентификатор вида спорта
  google.protobuf.Int64Value version = 3; // Версия данных у клиента. Поле не заполняется (null) при отсутствии данных.
}

message LiveSportEventsRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  int64 id = 2; // Идентификатор вида спорта
  google.protobuf.Int64Value version = 3; // Версия данных у клиента. Поле не заполняется (null) при отсутствии данных.
}

message LiveSportsRequest {
  int32 lang = 1; // Идентификатор языка (используется только 0 - русский)
  google.protobuf.Int64Value version = 2; // Версия данных у клиента. Поле не заполняется (null) при отсутствии данных.
}


// Сервис одиночного вызова методов для работы с лайвом.
service LiveService {

    // Метод возвращает информацию о чемпионате и его событиях с основными исходами
  rpc Champ(LiveChampRequest) returns (ChampEvents) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/champ/{id}"
    };
  }

    // Метод возвращает информацию о чемпионате и его событиях с основными исходами. Фактически Champ с периодом 0 и поддержкой версионирования.
  rpc ChampVersioned(LiveChampVersionedRequest) returns (ChampEventsVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/champ/{id}"
    };
  }

// Метод возвращает информацию о нескольких чемпионатах в остальном аналогичен [Champ](#Champ). 
  rpc Champs(LiveChampsRequest) returns (ChampsEventsVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/champs"
    };
  }

  // Метод возвращает информацию о всех доступных чемпионате линии по всем видам спорта.
  rpc ChampsAll(LiveChampsAllRequest) returns (SportsChampsVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/sport/all/champs"
    };
  }

  //  Метод возвращает информацию о событии со всеми исходами
  rpc Event(LiveEventRequest) returns (EventVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/event/{id}"
    };
  }

  /** Метод возвращает информацию для нескольких спортивных событий.
   *  В зависимости от параметров метод может выводить: 
   *  Все события для которых есть исходы из группы "основные"
   *  Заданную группу событий с основными / всеми исходами. 
   */
  rpc Events(LiveEventsRequest) returns (EventsVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/events"
    };
  }

  //  Метод возвращает информацию о виде спорта в линии (если для этого спорта есть матчи)
  rpc Sport(LiveSportRequest) returns (SportVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/sport/{id}"
    };
  }

  //  Метод возвращает информацию о чемпионатах котоыре сейчас есть в линии для данного вида спорта
  rpc SportChamps(LiveSportChampsRequest) returns (SportChampsVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/sport/{id}/champs"
    };
  }

  //  Метод возвращает информацию о событиях (и их основных исходах) для заданного вида спорта
  rpc SportEvents(LiveSportEventsRequest) returns (SportEventsVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/sport/{id}/events"
    };
  }

  //  Метод возвращает список с информацией о видах спорта представленных на текущий момент в лайве
  rpc Sports(LiveSportsRequest) returns (SportsVersionedResponse) {
    option (google.api.http) = {
      get: "/api/v2/{lang}/live/sports"
    };
  }

}

service LiveStreamService {
  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc Champ(LiveChampRequest) returns (stream ChampEvents);

  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc ChampVersioned(LiveChampVersionedRequest) returns (stream ChampEventsVersionedResponse);

  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc Champs(LiveChampsRequest) returns (stream ChampsEventsVersionedResponse);
  
  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc ChampsAll(LiveChampsAllRequest) returns (stream SportsChampsVersionedResponse);

  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc Event(LiveEventRequest) returns (stream EventVersionedResponse);

  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc Events(LiveEventsRequest) returns (stream EventsVersionedResponse);

  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc Sport(LiveSportRequest) returns (stream SportVersionedResponse);

  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc SportChamps(LiveSportChampsRequest) returns (stream SportChampsVersionedResponse);

  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc SportEvents(LiveSportEventsRequest) returns (stream SportEventsVersionedResponse);

  // Стриминговый метод аналогичен  одноименному в [LiveService](#olimpapi.LiveService)
  rpc Sports(LiveSportsRequest) returns (stream SportsVersionedResponse);
}
